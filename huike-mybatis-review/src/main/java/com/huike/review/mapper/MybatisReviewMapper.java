package com.huike.review.mapper;

import com.huike.review.pojo.Review;
import com.huike.review.vo.MybatisReviewVO;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * Mybatis复习的Mapper层
 */
public interface MybatisReviewMapper {





    /**======================================================新增======================================================**/
    void add(Review review);
    /**======================================================删除======================================================**/
    @Delete("delete from mybatis_review where id=#{id}")
    void delete(Integer id);
    /**======================================================修改======================================================**/
    void update(Review review);




    /**======================================================简单查询===================================================**/
    @Select("select * from mybatis_review where id=#{id}")
    Review getById(Integer id);

    @Select("select * from mybatis_review ")
    List<Review> getDataByPage();
}
