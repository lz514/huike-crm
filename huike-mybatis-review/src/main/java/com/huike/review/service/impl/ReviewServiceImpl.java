package com.huike.review.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.huike.common.core.domain.AjaxResult;
import com.huike.common.exception.CustomException;
import com.huike.common.utils.bean.BeanUtils;
import com.huike.review.pojo.Review;
import com.huike.review.service.ReviewService;
import com.huike.review.mapper.MybatisReviewMapper;
import com.huike.review.vo.MybatisReviewVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * mybatis复习使用的业务层
 * 注意该业务层和我们系统的业务无关，主要是让同学们快速熟悉系统的
 */
@Service
public class ReviewServiceImpl implements ReviewService {

    @Autowired
    private MybatisReviewMapper reviewMapper;



    /**=========================================================保存数据的方法=============================================*/

    @Override
    public void add(Review review) {
        reviewMapper.add(review);

    }


    /**=========================================================删除数据=============================================*/
    @Override
    public void delete(Integer id) {

        reviewMapper.delete(id);
    }



    /**=========================================================修改数据=============================================*/

    @Override
    public void update(Review review) {
        reviewMapper.update(review);
    }


    /**=========================================================查询数据的方法=============================================*/
    @Override
    public Review getById(Integer id) {
        Review review = reviewMapper.getById(id);
        return review;
    }

    @Override
    public PageInfo getDataByPage(Integer pageSize, Integer pageNum) {
        PageHelper.startPage(pageNum, pageSize);
        List<Review> reviews= reviewMapper.getDataByPage();
        PageInfo objectPageInfo = new PageInfo<>(reviews);
        return objectPageInfo;
    }

}
