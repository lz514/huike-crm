package com.huike.review.service;

import com.github.pagehelper.PageInfo;
import com.huike.review.pojo.Review;
import com.huike.review.vo.MybatisReviewVO;

import java.util.List;

/**
 * mybatis复习接口层
 */
public interface ReviewService {


    void add(Review review);

    void update(Review review);

    void delete(Integer id);

    Review getById(Integer id);

    PageInfo getDataByPage(Integer pageSize, Integer pageNum);
}
