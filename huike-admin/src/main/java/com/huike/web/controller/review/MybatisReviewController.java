package com.huike.web.controller.review;


import com.github.pagehelper.PageInfo;
import com.huike.common.core.controller.BaseController;
import com.huike.common.core.domain.AjaxResult;
import com.huike.review.pojo.Review;
import com.huike.review.service.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

/**
 * 该Controller主要是为了复习三层架构以及Mybatis使用的，该部分接口已经放开权限，可以直接访问
 * 同学们在此处编写接口通过浏览器访问看是否能完成最简单的增删改查
 */
@RestController
@RequestMapping("/review")
public class MybatisReviewController extends BaseController {

    @Autowired
    private ReviewService reviewService;

    /**=========================================================新增数据============================================*/

    @GetMapping("/saveData/{name}/{age}/{sex}")
    public AjaxResult add(@PathVariable String naem ,@PathVariable Integer age ,@PathVariable String sex) {
        Review review = new Review();
        review.setName(naem);
        review.setSex(sex);
        review.setAge(age);
        reviewService.add(review);
        return AjaxResult.success("成功插入:1条数据");
    }

    @PostMapping("/saveData")
    public AjaxResult add(@RequestBody Review review) {
        reviewService.add(review);
        return AjaxResult.success("成功插入:1条数据");
    }

    /**
     * =========================================================删除数据=============================================
     */
    @DeleteMapping("/remove/{id}")
    public AjaxResult delete(Integer id) {
        reviewService.delete(id);
        return AjaxResult.success("成功删除:1条数据");
    }

    /**=========================================================修改数据=============================================*/

    @PostMapping("/update")
    public AjaxResult update(@RequestBody Review review) {
        reviewService.update(review);
        return AjaxResult.success("成功修改:1条数据");

    }

    /**=========================================================查询数据=============================================*/
    @GetMapping("/getById")
    public AjaxResult getById(Integer id) {
        Review review = reviewService.getById(id);
        return AjaxResult.success(review);
    }

    @GetMapping("/getDataByPage")
    public AjaxResult getDataByPage(Integer pageSize,Integer pageNum) {
       PageInfo pageInfo= reviewService.getDataByPage(pageSize, pageNum);
        return AjaxResult.success(pageInfo);
    }


}