package com.huike.web.controller.common;

import com.huike.common.config.MinioTemplate;
import com.huike.common.core.controller.BaseController;
import com.huike.common.core.domain.AjaxResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * 通用请求处理
 */
@RestController
public class CommonController extends BaseController {
    private static final Logger log = LoggerFactory.getLogger(CommonController.class);


    @Autowired
    MinioTemplate minioTemplate;

    /**
     * 通用上传请求
     */
    @PostMapping("/common/upload")
    public AjaxResult uploadFile(MultipartFile file){
        try{
            // 上传文件路径
            String url = minioTemplate.fileUpload(file.getOriginalFilename(),file.getSize(),file.getContentType(),file.getInputStream());
            return AjaxResult.success(url);
        }catch (Exception e){
            return AjaxResult.error(e.getMessage());
        }
    }
}