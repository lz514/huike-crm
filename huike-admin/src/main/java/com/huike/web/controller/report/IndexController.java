package com.huike.web.controller.report;


import com.huike.common.utils.SecurityUtils;
import com.huike.report.domain.vo.IndexTodayInfoVO;
import com.huike.contract.domain.vo.indexBT;
import com.huike.report.domain.vo.IndexTodoInfoVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.huike.common.core.domain.AjaxResult;
import com.huike.report.service.IReportService;

import java.util.List;

@RestController
@RequestMapping("/index")
public class IndexController {

    @Autowired
    private IReportService reportService;


    /**
     * 首页--基础数据统计
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    @GetMapping("/getBaseInfo")
    public AjaxResult getBaseInfo(@RequestParam("beginCreateTime") String beginCreateTime,
                                  @RequestParam("endCreateTime") String endCreateTime){
        return AjaxResult.success(reportService.getBaseInfo(beginCreateTime,endCreateTime));
    }

    /**
     * 获取今日简报数据
     * @return
     */
    @GetMapping("/getTodayInfo")
    public AjaxResult getTodayInfo(){
        //获取当前登录用户id
        Long uid = SecurityUtils.getUserId();
        IndexTodayInfoVO indexTodayInfoVO=reportService.selectbyday(uid);
        return AjaxResult.success(indexTodayInfoVO);
    }


    /**
     *获取登录用户待跟进的线索/和所有待分配线索
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    @GetMapping("/getTodoInfo")
    public AjaxResult getTodoInfo(@RequestParam("beginCreateTime") String beginCreateTime,@RequestParam("endCreateTime") String endCreateTime) {
        IndexTodoInfoVO indexTodoInfoVO= reportService.getTodoInfo(beginCreateTime, endCreateTime);
        return AjaxResult.success(indexTodoInfoVO);
    }

}