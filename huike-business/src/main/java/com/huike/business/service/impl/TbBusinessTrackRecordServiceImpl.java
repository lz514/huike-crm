package com.huike.business.service.impl;


import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.huike.business.domain.TbBusiness;
import com.huike.business.domain.TbBusinessTrackRecord;
import com.huike.business.domain.vo.BusinessTrackVo;
import com.huike.business.mapper.TbBusinessMapper;
import com.huike.business.mapper.TbBusinessTrackRecordMapper;
import com.huike.business.service.ITbBusinessTrackRecordService;
import com.huike.common.utils.SecurityUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 商机跟进记录Service业务层处理
 * 
 * @author wgl
 * @date 2021-04-28
 */
@Service
public class TbBusinessTrackRecordServiceImpl implements ITbBusinessTrackRecordService {

    @Autowired
    TbBusinessMapper tbBusinessMapper;

    @Autowired
    TbBusinessTrackRecordMapper tbBusinessTrackRecordMapper;

    @Override
    public void insert(BusinessTrackVo businessTrackVo) {
        //获取当前登录人信息
        String username = SecurityUtils.getUsername();
        Long userId = SecurityUtils.getUserId();
        //获取当前时间
        Date date = new Date();
        //创建商机对象
        TbBusiness tbBusiness = new TbBusiness();
        //给商机对象赋值
        BeanUtils.copyProperties(businessTrackVo,tbBusiness);
        tbBusiness.setId(businessTrackVo.getBusinessId());//获取商机id
        tbBusiness.setUserId(userId);//添加修改人信息
        tbBusiness.setUpdateTime(date);//添加修改时间
        tbBusinessMapper.updateTbBusiness(tbBusiness);


        TbBusinessTrackRecord tbBusinessTrackRecord = new TbBusinessTrackRecord();
        BeanUtils.copyProperties(businessTrackVo,tbBusinessTrackRecord);
        tbBusinessTrackRecord.setCreateBy(username);
        tbBusinessTrackRecord.setCreateTime(date);
        tbBusinessTrackRecordMapper.insert(tbBusinessTrackRecord);





    }

    @Override
    public List select(Long id) {

        List list = tbBusinessTrackRecordMapper.select(id);


        return list;
    }
}
