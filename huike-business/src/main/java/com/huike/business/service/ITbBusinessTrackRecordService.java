package com.huike.business.service;

import com.github.pagehelper.PageInfo;
import com.huike.business.domain.vo.BusinessTrackVo;

import java.util.List;

/**
 * 商机跟进记录Service接口
 * @date 2021-04-28
 */
public interface ITbBusinessTrackRecordService {


    void insert(BusinessTrackVo businessTrackVo);

    List select(Long id);
}
