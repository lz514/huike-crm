package com.huike.business.mapper;

import java.util.List;
import com.huike.business.domain.TbBusinessTrackRecord;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * 商机跟进记录Mapper接口
 * @date 2021-04-28
 */
public interface TbBusinessTrackRecordMapper {

    void insert(TbBusinessTrackRecord tbBusinessTrackRecord);

    @Select("select * from tb_business_track_record where business_id=#{id}")
    List<TbBusinessTrackRecord> select(@Param("id") Long id);
}