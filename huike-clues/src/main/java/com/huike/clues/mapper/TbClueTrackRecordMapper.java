package com.huike.clues.mapper;


import com.huike.clues.domain.vo.ClueTrackRecordVo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 线索跟进记录Mapper接口
 * @date 2021-04-19
 */
public interface TbClueTrackRecordMapper {


    /**
     * 跟进记录表
     * @param tbClueTrackRecord
     */
    void insert(ClueTrackRecordVo tbClueTrackRecord);

    @Select("select * from tb_clue_track_record where clue_id=#{clueId} ")
    List<ClueTrackRecordVo> selectClueid(@Param("clueId") Long clueId);
}
