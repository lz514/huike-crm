package com.huike.clues.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.huike.clues.domain.TbClue;
import com.huike.clues.domain.TbClueTrackRecord;
import com.huike.clues.domain.vo.ClueTrackRecordVo;
import com.huike.clues.mapper.TbClueMapper;
import com.huike.clues.mapper.TbClueTrackRecordMapper;
import com.huike.clues.service.ITbCluetrackService;
import com.huike.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class ITbCluetrackServiceImpl implements ITbCluetrackService {
    //线索mapper
    @Autowired
    TbClueMapper tbClueMapper;

    //线索跟进记录Mapper
    @Autowired
    TbClueTrackRecordMapper tbClueTrackRecordMapper;


    @Override
    public void insert(ClueTrackRecordVo tbClueTrackRecord) {
        //获取登录人信息
        Long userId = SecurityUtils.getUserId();
        String username = SecurityUtils.getUsername();
        //获取当前时间
        Date date = new Date();
        //将前端对象拆分成2个对象,一个线索对象,一个线索跟进对象
        //创建线索对象
        TbClue tbClue = new TbClue();
        //给线索对象赋值
        tbClue.setUpdateBy(username);//修改人信息
        tbClue.setUpdateTime(date);
        tbClue.setId(tbClueTrackRecord.getClueId());//线索id
        tbClue.setName(tbClueTrackRecord.getName());//客户名称
        tbClue.setSex(tbClueTrackRecord.getSex());//客户性别
        tbClue.setAge(tbClueTrackRecord.getAge());//客户年龄
        tbClue.setWeixin(tbClueTrackRecord.getWeixin());//客户微信
        tbClue.setQq(tbClueTrackRecord.getQq());//客户qq
        tbClue.setLevel(tbClueTrackRecord.getLevel());//意向等级
        tbClue.setSubject(tbClueTrackRecord.getSubject());//意向学科
        tbClue.setNextTime(tbClueTrackRecord.getNextTime());//下次跟进时间

        //修改线索资料
        tbClueMapper.updateTbClue(tbClue);

        tbClueTrackRecord.setCreateBy(username);//添加操作人员信息
        tbClueTrackRecord.setCreateTime(date);//添加跟进时间
        //添加跟进记录
        tbClueTrackRecordMapper.insert(tbClueTrackRecord);
    }

    /**
     * 根据线索id查询记录
     * @param clueId
     * @return
     */
    @Override
    public PageInfo selectinfo(Long clueId) {
        List<ClueTrackRecordVo> list = tbClueTrackRecordMapper.selectClueid(clueId);
        PageInfo<ClueTrackRecordVo> clueTrackRecordVoPageInfo = new PageInfo<>(list);
        return clueTrackRecordVoPageInfo;
    }
}
