package com.huike.clues.service;


import com.github.pagehelper.PageInfo;
import com.huike.clues.domain.vo.ClueTrackRecordVo;

import java.util.List;

public interface ITbCluetrackService {

    void insert(ClueTrackRecordVo tbClueTrackRecord);

    PageInfo selectinfo(Long clueId);
}
