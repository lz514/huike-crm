package com.huike.clues.domain.vo;

import lombok.Data;

import java.util.List;

@Data
public class ClueindexVO {
    List<String> xAxis;
    List<CluezxtVO> series;
}
