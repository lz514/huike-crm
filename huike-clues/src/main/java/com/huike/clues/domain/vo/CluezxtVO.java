package com.huike.clues.domain.vo;

import lombok.Data;

import java.util.List;

@Data
public class CluezxtVO {
    String name;
    List<Integer> data;

}
